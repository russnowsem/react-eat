import React from 'react';

import logo from './logo.svg';

import './App.css';
import {Container, Navbar, Nav} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {TodoTableListContainer} from './components/TodoTableListContainer';
import TodoItemModel from './models/TodoItemModel';
import {TodoAddItem} from './components/TodoAddItem';
import {TimeContext} from "./components/TimeContext";

class App extends React.Component {

    constructor(props) {
        super(props);
        this.itemsModel = new TodoItemModel();
        this.state = {
            todoItems: [],
            addError: null,
            time: null,
        };
    }

    componentDidMount() {

        this.timeTimer = setInterval(()=>{this.updateTimeState()}, 1000);

        this.setState(
            {
                todoItems: this.itemsModel.getTodoItems(),
                addError: null,
                time: new Date().toLocaleString(),
            });
    }

    componentWillUnmount() {
        clearInterval(this.timeTimer);
    }

    updateTimeState = ()=> {
        this.setState({
            time: new Date().toLocaleString()
        })
    };

    addItemToStore = (title) => {

        if (title.length > 0) {

            if (this.itemsModel.addTodoItem(title)) {
                this.setState({
                    todoItems: [...this.itemsModel.getTodoItems()],
                    addError: null,
                });
                return true
            }
        } else {
            this.setState({
                addError: "Введите название"
            });
        }

        return false;

    };

    removeItem = (id) => {
        if (this.itemsModel.removeItem(id)) {
            this.setState({
                todoItems: [...this.itemsModel.getTodoItems()]
            })
        }
    };

    render() {
        return (
            <div className="App">
                <TimeContext.Provider value={this.state.time}>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Brand href="#home">
                        <img
                            alt=""
                            src={logo}
                            width="30"
                            height="30"
                            className="d-inline-block align-top"
                        />{' '}

                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className={""}>
                            <Nav.Link href="#deets">TODO APP</Nav.Link>
                            <Nav.Link href="#">
                                <TimeContext.Consumer>
                                    {(time)=> time}
                                </TimeContext.Consumer>
                            </Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

                <Container className={"mt-5 text-left"}>
                    <div className={"mt-3"}>
                        <TodoAddItem addError={this.state.addError} addItemToStoreHandler={this.addItemToStore}/>
                    </div>
                    <TodoTableListContainer handleDeleteItem={this.removeItem} todoItems={this.state.todoItems}/>
                </Container>
                </TimeContext.Provider>
            </div>
        );
    };
}

export default App;
