import {Button, InputGroup, FormControl} from "react-bootstrap";
import React from "react";
import {AlertError} from "./AlertError";


export class TodoAddItem extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            inputItem: ""
        };
    }

    componentDidMount() {
        this.setState(
            {
                inputItem: ""
            }
        )
    }

    handleAddClick = () => {
        if (this.props.addItemToStoreHandler(this.state.inputItem)) {
            this.setState({
                inputItem: ""
            });
        } else {
            console.log("Input error")
        }

        console.log('click')
    };

    handleChangeInputItem = (event) => {
        this.setState({
            inputItem: event.target.value
        });

        console.log('input item', this.state.inputItem)
    };

    render() {
        return (
            <div>
                <InputGroup className="mb-3">
                    <FormControl
                        placeholder="Введите задание"
                        aria-label="Recipient's username"
                        aria-describedby="basic-addon2"
                        onChange={this.handleChangeInputItem}
                        value={this.state.inputItem}
                    />
                    <InputGroup.Append>
                        <Button onClick={this.handleAddClick} variant={"primary"}>Добавить</Button>
                    </InputGroup.Append>
                </InputGroup>
                <AlertError error={this.props.addError}/>
            </div>
        );
    }
}

