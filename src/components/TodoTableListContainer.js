import React from 'react';
import {TodoTableListView} from './TodoTableListView';
import {TimeContext} from "./TimeContext";

export class TodoTableListContainer extends React.Component{

    handleDeleteItem = (id) => {
        this.props.handleDeleteItem(id);
        console.log(id)
    };

    render() {
        return (
            <div>
                <TimeContext.Consumer>
                    {(time)=>
                        <p className={"small"}>Время из контекста: {time}</p>

                    }
                </TimeContext.Consumer>

                <TodoTableListView handleDeleteItem={this.handleDeleteItem} todoItems={this.props.todoItems}>asd</TodoTableListView>
            </div>
        );
    }

}
