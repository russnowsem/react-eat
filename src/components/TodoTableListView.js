import {Button, Table} from 'react-bootstrap';
import React from 'react';

export class TodoTableListView extends React.Component {
    render() {
        return (
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Задача</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>
                {
                    this.props.todoItems.map(
                        (item, id) =>
                            <tr key={id}>
                                <td>{ id}</td>
                                <td>{ item.title}</td>
                                <td>
                                    <Button variant={"danger"} onClick={(e) => this.props.handleDeleteItem(id, e)}>Удалить</Button>
                                </td>
                            </tr>
                    )
                }
                </tbody>
            </Table>
        );
    }

}

